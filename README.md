### This is a fork of [ezdun](https://gitlab.com/dune-network/ezdun), deployed on [npm repository](https://www.npmjs.com/package/ezdun-lib)

![npm](https://img.shields.io/npm/v/ezdun-lib.svg?logo=npm&color=blue)

#### Getting Started
```js
npm install ezdun-lib
```
You can checkout our [Documentation](https://github.com/stephenandrews/eztz/wiki/Documentation), or follow installation below.

## Usage
Import ezdun-lib to your project:
```js
import ezdun from 'ezdun-lib'
```
Now you can use it, for example:
```
ezdun.rpc.getBalance("tz1LSAycAVcNdYnXCy18bwVksXci8gUC2YpA").then(function(res) {
    alert("Your balance is " + res);
}).catch(function(e) {
    console.log(e);
});
```

## License
MIT
